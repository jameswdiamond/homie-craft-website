$(document).ready(function(){
    // With JQuery

	var cw = $('.content-text').width();
	$('.content-text').css({'height':cw+'px', 'width': cw+'px'});

	$(window).on('scroll', function() {
	    var y_scroll_pos = window.pageYOffset + 130;
	    var scroll_pos_test = $('.sign-up-block').position().top;

	    if(window.matchMedia("(max-width: 768px)").matches){
	    	console.log('bbb');
	    	if(y_scroll_pos >= scroll_pos_test) {
	        	$('.btn-sign-in-header').addClass('btn-sign-in-header-show');
		    }else{
		    	$('.btn-sign-in-header').removeClass('btn-sign-in-header-show');
		    }
	    }else{
	    	if(y_scroll_pos >= scroll_pos_test) {
	        	$('.sign-in-link-header').addClass('sign-in-link-show');
		    }else{
		    	$('.sign-in-link-header').removeClass('sign-in-link-show');
		    }
	    }
	    
	});

	$('#rangeSlider').ionRangeSlider({
		skin: "round",
        grid: true,
        values: ["$10", "$50", "$100", "$150", "$200"]
    });

    $('#hero-sign-up').on('click', function(){
    	$('.sign-up-block').css('display', 'flex');
    })
})