<?php
namespace bundles;

class ScriptsBundle extends \craft\web\AssetBundle {
  public $sourcePath = '@bundles/scripts/dist';
  public $js = ['scripts.min.js'];
}