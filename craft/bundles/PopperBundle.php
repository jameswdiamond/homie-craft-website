<?php
namespace bundles;

class PopperBundle extends \craft\web\AssetBundle {
public $sourcePath = '@node/popper.js/dist/umd';
public $js = ['popper.min.js'];
}