<?php
namespace bundles;

class RangeSliderBundle extends \craft\web\AssetBundle {
  public $sourcePath = '@node/ion-rangeslider';
  public $js = ['js/ion.rangeSlider.min.js'];
  public $css = ['css/ion.rangeSlider.min.css'];
}