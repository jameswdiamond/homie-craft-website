<?php
namespace bundles;

class JqueryBundle extends \craft\web\AssetBundle {
  public $sourcePath = '@node/jquery/dist';
  public $js = ['jquery.min.js'];
}