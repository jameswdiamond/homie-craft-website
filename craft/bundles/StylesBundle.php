<?php
namespace bundles;

class StylesBundle extends \craft\web\AssetBundle {
  public $sourcePath = '@bundles/styles';
  public $css = ['dist/index.min.css'];
}