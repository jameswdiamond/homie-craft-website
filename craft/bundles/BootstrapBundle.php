<?php
namespace bundles;

use bundles\PopperBundle;
use bundles\JqueryBundle;

class BootstrapBundle extends \craft\web\AssetBundle {
  public $depends = [JqueryBundle::class, PopperBundle::class, ];
  public $sourcePath = '@node/bootstrap/dist';
  public $js = ['js/bootstrap.bundle.min.js'];
  public $css = ['css/bootstrap.min.css'];
}