<?php
namespace bundles;

class JqueryUIBundle extends \craft\web\AssetBundle {
  public $sourcePath = '@node/jquery-ui-dist';
  public $js = ['jquery-ui.min.js'];
  public $css = ['jquery-ui.min.css'];
}