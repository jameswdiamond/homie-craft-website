// Load in all required packages
const gulp = require('gulp');
const pkg = require('./package.json');
const path = require('path');
const $ = require('gulp-load-plugins')({
  pattern: ['*'],
  scope: ['devDependencies']
});

const stylesPath = path.join(__dirname, 'bundles/styles');
const scriptsPath = path.join(__dirname, 'bundles/scripts');
const compPath = path.join(__dirname, 'bundles/components');

//Setting sass compiler
$.sass.compiler = require('node-sass');

_compileStyles = () => {    
  return gulp.src(pkg.paths.scss.src)
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.csso())
    .pipe($.rename({extname: '.min.css'}))
    .pipe(gulp.dest(pkg.paths.scss.dist));
}

_compileScripts = () => {
  return gulp.src(pkg.paths.js.src)
    .pipe($.concat(pkg.vars.scriptName))
    .pipe($.babel({presets: ['@babel/env']}))
    .pipe($.uglify())
    .pipe($.rename({extname:'.min.js'}))
    .pipe(gulp.dest(pkg.paths.js.dist));
}

_compileComponentStyles = () => {
  return gulp.src(pkg.paths.scss.componentSrc)
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.csso())
    .pipe($.rename({extname: '.min.css'}))
    .pipe(gulp.dest(pkg.paths.scss.componentDist));
}

_compileComponentScripts = () => {
  return gulp.src(pkg.paths.js.componentSrc)
    .pipe($.babel({presets: ['@babel/env']}))
    .pipe($.uglify())
    .pipe($.rename({extname: '.min.js'}))
    .pipe(gulp.dest(pkg.paths.js.componentDist));
}

gulp.task('scss', done => {
  $.fancyLog('-> Compiling styles');
  return _compileStyles();
  done();
});

gulp.task('js', done => {
  $.fancyLog('-> Compiling scripts');
  return _compileScripts();
  done();
});

gulp.task('scssComp', done => {
  $.fancyLog('-> Compiling styles');
  return _compileComponentStyles();
  done();
});

gulp.task('jsComp', done => {
  $.fancyLog('-> Compiling scripts');
  return _compileComponentScripts();
  done();
});

gulp.task('watch', function(done){
    gulp.watch(stylesPath + "/src/**/*.scss", _compileStyles);
    gulp.watch(scriptsPath + "/src/**/*.js", _compileScripts);
    gulp.watch(compPath + "/src/**/*.scss", _compileComponentStyles);
    gulp.watch(compPath + "/src/**/*.js", _compileComponentScripts);
    done();
});

gulp.task('default', gulp.series('scss', 'js', 'scssComp', 'jsComp'));
