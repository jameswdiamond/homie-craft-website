<?php
/**
 * Database Configuration
 *
 * All of your system's database connection settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/DbConfig.php.
 *
 * @see craft\config\DbConfig
 */

use craft\helpers\App;

return [
    'dsn' => App::env('MYSQL_DSN') ?: null,
    'driver' => App::env('MYSQL_DRIVER'),
    'server' => App::env('MYSQL_SERVER'),
    'port' => App::env('MYSQL_PORT'),
    'database' => App::env('MYSQL_DATABASE'),
    'user' => App::env('MYSQL_USER'),
    'password' => App::env('MYSQL_PASSWORD'),
    'schema' => App::env('MYSQL_SCHEMA'),
    'tablePrefix' => App::env('MYSQL_TABLE_PREFIX'),
];
